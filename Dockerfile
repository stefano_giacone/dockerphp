FROM php:7.1.12-apache

#install wget and cron
RUN apt-get update && apt-get install -y wget cron

#install mysqli
RUN docker-php-ext-install -j$(nproc) mysqli
#install pdo-mysql
RUN docker-php-ext-install -j$(nproc) pdo pdo_mysql
#install gettext
RUN docker-php-ext-install -j$(nproc) gettext
#install zip
RUN apt-get install -y zip zlib1g-dev
RUN docker-php-ext-install -j$(nproc) zip
# hashids 2.0.3 requires ext-bcmath
RUN docker-php-ext-install -j$(nproc) bcmath
#install exif
RUN docker-php-ext-install -j$(nproc) exif
#install sockets
RUN docker-php-ext-install -j$(nproc) sockets
#install gd and configure jpeg/webp support
RUN apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng12-dev libwebp-dev
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-webp-dir=/usr/include/ && docker-php-ext-install -j$(nproc) gd
# enable apache mod_rewrite
RUN a2enmod rewrite
#install logrotate
RUN apt-get install -y logrotate
#configure logrotate
#ADD BuildSupportFiles/Docker/logrotate_laravel /etc/logrotate.d/logrotate_laravel

#install composer
ADD BuildSupportFiles/Docker/install-composer.sh /
RUN /install-composer.sh
RUN rm /install-composer.sh
RUN mv /var/www/html/composer.phar /usr/local/bin/composer

#add the custom php.ini
ADD BuildSupportFiles/Docker/php.ini /usr/local/etc/php/

#install phpredis
RUN wget https://pecl.php.net/get/redis-3.1.4.tgz
RUN tar -xvf redis-3.1.4.tgz
RUN cd redis-3.1.4 && phpize && ./configure && make && make install
RUN rm -rf redis-3.1.4
RUN rm redis-3.1.4.tgz
RUN rm package.xml

#install intl
RUN apt-get install -y zlib1g-dev libicu-dev g++ \
  && docker-php-ext-configure intl \
  && docker-php-ext-install intl

#install igbinary
RUN wget https://pecl.php.net/get/igbinary-2.0.4.tgz
RUN tar -xvf igbinary-2.0.4.tgz
RUN cd igbinary-2.0.4 && phpize && ./configure CFLAGS="-O2 -g" --enable-igbinary && make && make install
RUN rm -rf igbinary-2.0.4
RUN rm igbinary-2.0.4.tgz
RUN rm package.xml

#Install phpunit
RUN wget https://phar.phpunit.de/phpunit.phar
RUN chmod +x phpunit.phar
RUN mv phpunit.phar /usr/local/bin/phpunit

#installing dumb-init: https://github.com/Yelp/dumb-init (A minimal init system for Linux containers)
RUN wget https://github.com/Yelp/dumb-init/releases/download/v1.2.1/dumb-init_1.2.1_amd64.deb
RUN dpkg -i dumb-init_*.deb